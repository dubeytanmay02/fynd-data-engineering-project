import json
import ast
import time
from confluent_kafka import Consumer, Producer
from confluent_kafka.admin import AdminClient, NewTopic

from config import ZOOKEEPER_STRING, BOOTSTRAP_BROKER_STRING

class KafkaHandler():
    def __init__(self):
        self.zookeeper_string=ZOOKEEPER_STRING
        self.bootstrap_broker_string=BOOTSTRAP_BROKER_STRING
        self.kafka_configuration = {
            'bootstrap.servers' : self.bootstrap_broker_string
        }
        self.producer = Producer(self.kafka_configuration)
        self.kafka_admin_client = AdminClient(self.kafka_configuration)

        self.kafka_configuration.update({
            'group.id' : 'ConsumerGroup1',
            'auto.offset.reset' : 'earliest'
        })
        self.consumer = Consumer(self.kafka_configuration)
    
    def delivery_report(self, err, msg):
        if err is not None:
            print('Message delivery failed: {}'.format(err))
        else:
            print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))
    
    def kafka_producer(self, topic_name, data):
        count=1
        if type(data) == list:
            for data_item in data:
                self.producer.poll(0)
                self.producer.produce(
                    topic_name, 
                    json.dumps(data_item).encode('utf-8'), 
                    callback=self.delivery_report
                )
            self.producer.flush(timeout=5)
            count = len(data)
        else:
            self.producer.poll(0)
            self.producer.produce(
                topic_name, 
                json.dumps(data).encode('utf-8'), 
                callback=self.delivery_report
            )
            self.producer.flush(timeout=5)
        
        print(f'{count} records produced successfully...')
    
    def kafka_consumer(self, topic_name, seconds=1):
        received_payload = list()
        self.consumer.subscribe([topic_name])
        count=0
        while count < seconds:
            msg = self.consumer.poll(1.0)
            if msg is None:
                count += 1
                continue
            if msg.error():
                print(f'Consumer Error: {msg.error()}')
                continue
            
            received_payload.append(ast.literal_eval(msg.value().decode("utf-8")))
            count=0
        return received_payload
    
    def create_kafka_topics(self, topic_name_list):
        kafka_topic_list = list(self.kafka_admin_client.list_topics().topics.keys())
        topics_for_creation = [
            NewTopic(topic_name, 1, 1) 
            for topic_name in topic_name_list 
            if topic_name not in kafka_topic_list
        ]
        if len(topics_for_creation):
            self.kafka_admin_client.create_topics(topics_for_creation)
            time.sleep(2)

