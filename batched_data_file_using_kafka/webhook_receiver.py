from flask import Flask, request, abort

from config import HTTP_WEBHOOK_URL
from utils import payload_handler
from kafka_handler import KafkaHandler

app = Flask(__name__)
kafka = KafkaHandler()

webhook_uri = HTTP_WEBHOOK_URL.split('/')[-1]

@app.route(f'/{webhook_uri}', methods=['POST'])
def webhook():
    if request.method == 'POST':
        payload = request.json
        
        # Payload handling here...
        print(payload)
        event_dict = payload_handler(payload=payload)
        kafka.create_kafka_topics(list(event_dict.keys()))
        for event_type, event_payload in event_dict.items():
            kafka.kafka_producer(topic_name=event_type, data=event_payload)
        
        return 'success', 200
    else:
        abort(400)


if __name__ == '__main__':
    app.run()