HTTP_WEBHOOK_URL = 'http://localhost:5000/kafkaProducerWebHook'

ZOOKEEPER_STRING = 'localhost:2181'

BOOTSTRAP_BROKER_STRING = 'localhost:9092'

PATH_TO_DATA = '/tmp'
DATA_FILE_DIR_NAME = 'data_file_dir'

# Time constraint for data files in seconds...
TIME_CONSTRAINT = 30

# Size constraint for data files in KBs...
SIZE_CONSTRAINT = 10

FILE_NAME_DATE_FORMAT = '%Y_%m_%d_%H_%M_%S_%f'