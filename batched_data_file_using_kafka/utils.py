import copy

def payload_handler(payload):
    event_dict = dict()
    for event in payload:
        if event['event_type'] not in event_dict.keys():
            event_dict[event['event_type']] = [copy.deepcopy(event)]
        else:
            event_dict[event['event_type']].append(copy.deepcopy(event))
    
    return event_dict