import os
import json
import time
import datetime

from config import PATH_TO_DATA, DATA_FILE_DIR_NAME, TIME_CONSTRAINT, SIZE_CONSTRAINT, FILE_NAME_DATE_FORMAT
from kafka_handler import KafkaHandler


class ConsumerDataHandler():
    def __init__(self):
        self.kafka = KafkaHandler()
        self.consumer_topic_dict = dict()
    
    def get_topic_objects(self):
        self.topic_list = [topic for topic in self.kafka.kafka_admin_client.list_topics().topics.keys() if topic != '__consumer_offsets']
        for topic_name in self.topic_list:
            print(f'Consuming {topic_name}...')
            os.makedirs(os.path.join(PATH_TO_DATA, DATA_FILE_DIR_NAME, topic_name), exist_ok=True)
            topic_payload = self.kafka.kafka_consumer(topic_name=topic_name)
            if topic_name not in self.consumer_topic_dict.keys():
                self.consumer_topic_dict[topic_name] = {
                    'time': time.time(),
                    'payload': [event for event in topic_payload],
                    'size': len(json.dumps([event for event in topic_payload], indent=4))/1024
                    }
            else:
                if (self.consumer_topic_dict[topic_name]['size'] > SIZE_CONSTRAINT) or ((time.time()-self.consumer_topic_dict[topic_name]['time']) > TIME_CONSTRAINT):
                    if len(self.consumer_topic_dict[topic_name]['payload']):
                        file_name = f'{topic_name}_{datetime.datetime.now().strftime(FILE_NAME_DATE_FORMAT)}.json'
                        path_to_save_data = os.path.join(PATH_TO_DATA, DATA_FILE_DIR_NAME, topic_name, file_name)
                        with open(path_to_save_data, 'w') as fw:
                            fw.write(json.dumps(self.consumer_topic_dict[topic_name]['payload'], indent=4))
                    self.consumer_topic_dict[topic_name] = {
                        'time': time.time(),
                        'payload': [event for event in topic_payload],
                        'size': len(json.dumps([event for event in topic_payload], indent=4))/1024
                        }
                else:
                    self.consumer_topic_dict[topic_name]['payload'] += topic_payload
                    self.consumer_topic_dict[topic_name]['size'] = len(json.dumps(self.consumer_topic_dict[topic_name]['payload'], indent=4))/1024

    def start_consumer_service(self):
        while True:
            self.get_topic_objects()
            print(f'Consumer Topic Dictionary Size: {len(json.dumps(self.consumer_topic_dict, indent=4))/1024} KBs')



if __name__ == '__main__':
    consumer = ConsumerDataHandler()
    consumer.start_consumer_service()