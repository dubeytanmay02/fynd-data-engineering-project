import logging
import logging.config

log_formatter = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(format=log_formatter, level=logging.INFO)

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
})

logger = logging.getLogger(__name__)