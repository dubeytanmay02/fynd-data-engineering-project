import random
import string

event_list = [
    'testEvent1', 'testEvent2', 'testEvent3', 'testEvent4',
    'testEvent5', 'testEvent6', 'testEvent7', 'testEvent8',
    'testEvent9', 'testEvent10', 'testEvent11', 'testEvent12',
    'testEvent13', 'testEvent14', 'testEvent15', 'testEvent16',
    'testEvent17', 'testEvent18', 'testEvent19', 'testEvent20'
    ]

data_keys = list(string.ascii_lowercase)

data_values = list(range(1, 101))

def get_random_data():
    data = list()
    event_type_list = random.sample(event_list, random.choice(range(1, len(event_list)+1)))
    for event_type in event_type_list:
        points = random.choice(range(1, 11))
        keys = random.sample(data_keys, points)
        values = random.sample(data_values, points)
        data.append({
            'event_type': event_type,
            'data': dict(zip(keys, values))
        })
    return data

if __name__ == '__main__':
    print(get_random_data())

