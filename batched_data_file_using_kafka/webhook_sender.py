import requests
import json

from config import HTTP_WEBHOOK_URL
from fake_data_generator import get_random_data


data = get_random_data()

req = requests.post(
    HTTP_WEBHOOK_URL, 
    data=json.dumps(data), 
    headers={'Content-Type': 'application/json'}
    )