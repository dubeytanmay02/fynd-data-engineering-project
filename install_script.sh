#! /bin/bash

wget https://dlcdn.apache.org/kafka/3.0.0/kafka_2.13-3.0.0.tgz -O kafka.tgz
tar -xzf kafka.tgz
rm kafka.tgz

KAFKA_DIR_NAME="kafka"
ZOOKEEPER_SERVICE_FILE_NAME="zookeeper.service"
KAFKA_SERVICE_FILE_NAME="kafka.service"
KAFKA_HOME="$HOME/$KAFKA_DIR_NAME"

PYTHON_HOME=$(dirname $(which python3))
CODE_DIR_NAME="fynd_task"
PYTHON_WEBHOOK_RECEIVER_SERVICE_FILE_NAME="python_webhook_receiver.service"
PYTHON_DATA_FILE_GENERATOR_SERVICE_FILE_NAME="python_data_file_generator.service"

mkdir ~/$KAFKA_DIR_NAME
mv kafka*/* ~/$KAFKA_DIR_NAME
rm -rf kafka*

mkdir ~/$CODE_DIR_NAME
cp -r batched_data_file_using_kafka/* ~/$CODE_DIR_NAME/

cat <<EOF >> ~/.bashrc

# KAFKA ENVIRONMENT VARIABLES FROM SCRIPT
export KAFKA_HOME=\$HOME/$KAFKA_DIR_NAME
export PATH=\$PATH:\$KAFKA_HOME/bin
EOF

cat <<EOF >> $ZOOKEEPER_SERVICE_FILE_NAME
[Unit]
Description=Bootstrapped Zookeeper
After=syslog.target network.target

[Service]
Type=simple
User=root
Environment="PATH=$JAVA_HOME/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$KAFKA_HOME/bin"
ExecStart=$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties
ExecStop=$KAFKA_HOME/bin/zookeeper-server-stop.sh

[Install]
WantedBy=multi-user.target
EOF

cat <<EOF >> $KAFKA_SERVICE_FILE_NAME
[Unit]
Description=Apache Kafka
Requires=$ZOOKEEPER_SERVICE_FILE_NAME
After=$ZOOKEEPER_SERVICE_FILE_NAME

[Service]
Type=simple
User=root
Environment="PATH=$JAVA_HOME/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$KAFKA_HOME/bin"
ExecStart=$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties
ExecStop=$KAFKA_HOME/bin/kafka-server-stop.sh

[Install]
WantedBy=multi-user.target
EOF

cat <<EOF >> $PYTHON_WEBHOOK_RECEIVER_SERVICE_FILE_NAME
[Unit]
Description=Python Webhook Receiver
After=syslog.target network.target

[Service]
Type=simple
User=root
Environment="PATH=$JAVA_HOME/bin:$PYTHON_HOME:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$KAFKA_HOME/bin"
ExecStart=$PYTHON_HOME/python3 $HOME/fynd_task/webhook_receiver.py

[Install]
WantedBy=multi-user.target
EOF

cat <<EOF >> $PYTHON_DATA_FILE_GENERATOR_SERVICE_FILE_NAME
[Unit]
Description=Python Data File Generator
After=syslog.target network.target

[Service]
Type=simple
User=root
Environment="PATH=$JAVA_HOME/bin:$PYTHON_HOME:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$KAFKA_HOME/bin"
ExecStart=$PYTHON_HOME/python3 $HOME/fynd_task/data_file_generator.py

[Install]
WantedBy=multi-user.target
EOF

sudo mv $ZOOKEEPER_SERVICE_FILE_NAME $KAFKA_SERVICE_FILE_NAME $PYTHON_WEBHOOK_RECEIVER_SERVICE_FILE_NAME $PYTHON_DATA_FILE_GENERATOR_SERVICE_FILE_NAME /lib/systemd/system/