# Fynd Data Engineering Project



## Getting started

Just follow the instructions in **setup.txt** to run the kafka and python services and use **webhook_sender.py** to send the data to kafka cluster...


## Prerequisites

- One Ubuntu server and a non-root user with sudo privileges.
- At least 4GB of RAM on your server.
- Java and Python installed on your server.
